import weka.associations.Apriori;
import weka.associations.AssociationRule;
import weka.associations.AssociationRules;
import weka.associations.Item;
import weka.associations.NominalItem;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class WekaRuleMinerApplication {

    private static final String INPUT_DATA_FILE_PATH = "weka_in.arff";

    public static void main(String[] args) throws Exception {
        new WekaRuleMinerApplication().run();
    }

    private void run() throws Exception {
        Apriori apriori = configApriori(loadDataSet());

        AssociationRules associationRules = apriori.getAssociationRules();

        List<String> allRulesAsString = convertRulesToString(associationRules.getRules());
        System.out.println(("Found " + associationRules.getRules().size() + " rules."));
        Files.write(createFile("all_rules").toPath(), allRulesAsString);

//        List<AssociationRule> violenceRules = filterRules(associationRules, attrValue -> attrValue.contains("violence"));
//        List<String> violenceRulesAsString = convertRulesToString(violenceRules);
//        Files.write(createFile("violence_rules").toPath(), violenceRulesAsString);
    }

    private Instances loadDataSet() throws IOException {
        System.out.println("Loading data set.");
        ArffLoader loader = new ArffLoader();
        loader.setFile(new File(INPUT_DATA_FILE_PATH));
        return loader.getDataSet();
    }

    private Apriori configApriori(Instances dataSet) throws Exception {
        System.out.println("Configuring apriori.");
        Apriori apriori = new Apriori();
        apriori.setNumRules(200_000);
        apriori.setLowerBoundMinSupport(0.0);
        System.out.println("Mining rules");
        apriori.buildAssociations(dataSet);
        return apriori;
    }

    private File createFile(String fileName) throws IOException {
        System.out.println("Saving rules to file: [" + fileName + "]");
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }

        if (!file.canWrite()) {
            file.setWritable(true);
        }
        return file;
    }

    private List<String> convertRulesToString(List<AssociationRule> rules) {
        return rules.stream()
                .map(r -> r.getPremise().stream().map(p -> p.getAttribute().name() + "='" + p.getItemValueAsString() + "'").collect(Collectors.joining(" "))
                        + " ==> "
                        + r.getConsequence().stream().map(p -> p.getAttribute().name() + "='" + p.getItemValueAsString() + "'").collect(Collectors.joining(" "))
                        + " #SUP: " + r.getTotalSupport() + " #CONF: " + ((double) r.getTotalSupport() / (double) r.getPremiseSupport()))
                .collect(Collectors.toList());
    }

    private List<AssociationRule> filterRules(AssociationRules associationRules, Predicate<String> predicate) {
        List<AssociationRule> violenceRules = new ArrayList<>();
        for (AssociationRule rule : associationRules.getRules()) {
            for (Item item : rule.getConsequence()) {
                if (item instanceof NominalItem) {
                    NominalItem nominalItem = (NominalItem) item;
                    String attrValue = item.getAttribute().value(nominalItem.getValueIndex());
                    if (predicate.test(attrValue)) {
                        violenceRules.add(rule);
                    }
                }
            }
        }
        return violenceRules;
    }
}
