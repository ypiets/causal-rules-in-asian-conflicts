package elastic.service;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class RuleImportServiceTest {

    @Test
    public void testConfidence() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String rule = "ALLY_ACTOR_1='The Islami Chhatra Shibir' ==> EVENT_TYPE=Riots/Protests #SUP: 27 #CONF: 0.7297297297297297";

        RuleImportService ruleImportService = new RuleImportService(null, null);
        Method parseSPMFConfidenceMethod = ruleImportService.getClass().getDeclaredMethod("parseSPMFConfidence", String.class);
        parseSPMFConfidenceMethod.setAccessible(true);
        Double invoke = (Double) parseSPMFConfidenceMethod.invoke(ruleImportService, rule);
        assertEquals(invoke, 0.7297297297297297, 0.001);
    }

    @Test
    public void testConfidence2() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        String rule = "EVENT_TYPE=Riots/Protests ==> ALLY_ACTOR_1='The Islami Chhatra Shibir' #SUP: 27 #CONF: 9.746236869653106E-4";
        RuleImportService ruleImportService = new RuleImportService(null, null);
        Method parseSPMFConfidenceMethod = ruleImportService.getClass().getDeclaredMethod("parseSPMFConfidence", String.class);
        parseSPMFConfidenceMethod.setAccessible(true);
        Double invoke = (Double) parseSPMFConfidenceMethod.invoke(ruleImportService, rule);
        assertEquals(invoke, 0.0009746236869653106, 0.001);
    }

}