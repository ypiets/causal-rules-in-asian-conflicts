package elastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuleSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuleSearchApplication.class, args);
	}
}
