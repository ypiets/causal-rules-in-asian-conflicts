package elastic.repository;

import elastic.document.RuleDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface RuleRepository extends ElasticsearchRepository<RuleDocument, Long> {
}
