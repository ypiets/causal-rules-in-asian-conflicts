package elastic.event;

import org.springframework.context.ApplicationEvent;

public class RulesReadyEvent extends ApplicationEvent {

    public RulesReadyEvent(Object source) {
        super(source);
    }
}
