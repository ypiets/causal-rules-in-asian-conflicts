package elastic.controller;

import elastic.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/statistics")
@ConditionalOnProperty("prepare.statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;

    @GetMapping
    public ResponseEntity<?> statistics() {
        List<StatisticsService.StatisticData> statistics = statisticsService.getStatistics();
        if (statistics.isEmpty()) {
            return ResponseEntity.status(421).body("Statistics is not ready yet.\n");
        }
        return ResponseEntity.ok(statistics);
    }
}
