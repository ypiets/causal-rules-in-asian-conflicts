package elastic.controller;

import elastic.document.RuleDocument;
import elastic.repository.RuleRepository;
import elastic.service.RuleImportService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/rules")
public class RuleController {

    private static final String MIN_CONFIDENCE_PARAMETER = "min-confidence";
    private static final String MIN_SUPPORT_PARAMETER = "min-support";

    private final RuleRepository ruleRepository;

    private final RuleImportService ruleImportService;

    @Value("${spring.data.elasticsearch.query.url}")
    private String elasticClusterUrl;

    @GetMapping(path = "/{id}")
    public ResponseEntity<RuleDocument> rule(@PathVariable Long id) {
        return ResponseEntity.of(ruleRepository.findById(id));
    }

    @GetMapping(path = "/search")
    public ResponseEntity<?> ruleByQuery(
            @RequestParam(required = false) List<String> premise,
            @RequestParam(required = false) List<String> consequence,
            @RequestParam(required = false) List<String> contains,
            @RequestParam(required = false) List<String> source,
            @RequestParam(required = false) List<String> algorithm,
            @RequestParam(required = false, name = MIN_CONFIDENCE_PARAMETER, defaultValue = "0") Double minConfidence,
            @RequestParam(required = false, name = MIN_SUPPORT_PARAMETER, defaultValue = "0") Integer minSupport
    ) {

        String query = prepareQuery(premise, consequence, contains, source, algorithm, minConfidence, minSupport);
        Object hits = postToElastic(query);
        return ResponseEntity.ok(hits);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteAll() {
        long count = ruleRepository.count();
        ruleRepository.deleteAll();
        return ResponseEntity.ok(count);
    }

    @PostMapping(path = "/migrate")
    public ResponseEntity<?> migrate() throws IOException {
        ruleImportService.migrate();
        RuleDocument.generatedId = 0L;
        return ResponseEntity.accepted().build();
    }

    private Object postToElastic(String query) {
        RestTemplate rest = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(query, headers);
        Map response = rest.postForObject("http://" + elasticClusterUrl + " /_search", entity, Map.class);

        return response.get("hits");
    }

    private String prepareQuery(List<String> premise, List<String> consequence, List<String> contains, List<String> source, List<String> algorithm, Double minConfidence, Integer minSupport) {
        return "{" +
                "  \"from\": 0, \"size\": 1000," +
                "   \"sort\" : [\n" +
                "        { \"_score\" : \"desc\" },\n" +
                "        { \"support\" : \"desc\" },\n" +
                "        { \"confidence\" : \"desc\" }\n" +
                "    ]," +
                "  \"query\": {" +
                "    \"bool\": {" +
                "      \"must\": [" +
                "        {" +
                "          \"bool\": {" +
                "            \"should\": [" +
                appendValues(RuleDocument.SOURCE_PROPERTY, source) +
                "            ]," +
                "            \"adjust_pure_negative\": true," +
                "            \"boost\": 1.0" +
                "          }" +
                "        }," +
                "        {" +
                "          \"bool\": {" +
                "            \"should\": [" +
                appendValues(RuleDocument.PREMISE_PROPERTY, premise) +
                "            ]," +
                "            \"adjust_pure_negative\": true," +
                "            \"boost\": 1.0" +
                "          }" +
                "        }," +
                "        {" +
                "          \"bool\": {" +
                "            \"should\": [" +
                appendValues(RuleDocument.CONSEQUENCE_PROPERTY, consequence) +
                "            ]," +
                "            \"adjust_pure_negative\": true," +
                "            \"boost\": 1.0" +
                "          }" +
                "        }," +
                "        {" +
                "          \"bool\": {" +
                "            \"should\": [" +
                appendValues(RuleDocument.FULL_TEXT_PROPERTY, contains) +
                "            ]," +
                "            \"adjust_pure_negative\": true," +
                "            \"boost\": 1.0" +
                "          }" +
                "        }," +
                "        {" +
                "          \"bool\": {" +
                "            \"should\": [" +
                appendValues(RuleDocument.ALGORITHM_PROPERTY, algorithm) +
                "            ]," +
                "            \"adjust_pure_negative\": true," +
                "            \"boost\": 1.0" +
                "          }" +
                "        }," +
                "        {" +
                "          \"range\": {" +
                "            \"" + RuleDocument.CONFIDENCE_PROPERTY + "\": {" +
                "              \"from\":" + minConfidence + "," +
                "              \"to\": null," +
                "              \"include_lower\": true," +
                "              \"include_upper\": true," +
                "              \"boost\": 1.0" +
                "            }" +
                "          }" +
                "        }," +
                "        {" +
                "          \"range\": {" +
                "            \"" + RuleDocument.SUPPORT_PROPERTY + "\": {" +
                "              \"from\":" + minSupport + "," +
                "              \"to\": null," +
                "              \"include_lower\": true," +
                "              \"include_upper\": true," +
                "              \"boost\": 1.0" +
                "            }" +
                "          }" +
                "        }" +
                "      ]," +
                "      \"adjust_pure_negative\": true," +
                "      \"boost\": 1.0" +
                "    }" +
                "  }" +
                "}";
    }

    private String appendValues(String fieldName, List<String> values) {
        if (ObjectUtils.isEmpty(values)) {
            return StringUtils.EMPTY;
        }
        String query = StringUtils.EMPTY;
        for (String value : values) {
            query += "{" +
                    "  \"query_string\": {" +
                    "    \"query\": \"" + value + "\"," +
                    "    \"fields\": [" +
                    "      \"" + fieldName + "^1.0\"" +
                    "    ]," +
                    "    \"type\": \"best_fields\"," +
                    "    \"default_operator\": \"or\"," +
                    "    \"max_determinized_states\": 10000," +
                    "    \"enable_position_increments\": true," +
                    "    \"fuzziness\": \"AUTO\"," +
                    "    \"fuzzy_prefix_length\": 0," +
                    "    \"fuzzy_max_expansions\": 50," +
                    "    \"phrase_slop\": 0," +
                    "    \"escape\": false," +
                    "    \"auto_generate_synonyms_phrase_query\": true," +
                    "    \"fuzzy_transpositions\": true," +
                    "    \"boost\": 1.0" +
                    "  }" +
                    "},";
        }
        return query.substring(0, query.length() - 1);
    }
}
