package elastic.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.opencsv.CSVReader;
import elastic.document.RuleDocument;
import elastic.document.RuleDocument.MineAlgorithm;
import elastic.document.RuleDocument.RuleSource;
import elastic.event.RulesReadyEvent;
import elastic.repository.RuleRepository;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static elastic.document.RuleDocument.CONSEQUENCE_PROPERTY;
import static elastic.document.RuleDocument.PREMISE_PROPERTY;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty("prepare.statistics")
public class StatisticsService {

    private final static int EVENT_TYPE_INDEX = 0;
    private final static int ACTOR1_INDEX = 1;
    private final static int ACTOR2_INDEX = 2;
    private static final String EMPTY_VALUE = "?";

    private final Logger logger = LogManager.getLogger(this.getClass());

    @Value("${data.path}")
    private String dataPath;

    private final RuleRepository ruleRepository;

    @Getter
    private final List<StatisticData> statistics = new LinkedList<>();

    @Async
    @EventListener(classes = RulesReadyEvent.class)
    public void prepareStatistics() throws IOException {
        logger.info("Begin to calculate statistics.");
        Resource resource = new ClassPathResource(dataPath);
        Reader reader = Files.newBufferedReader(resource.getFile().toPath());
        CSVReader csvReader = new CSVReader(reader);

        StatisticData wekaStatistic = new StatisticData(RuleSource.WEKA, MineAlgorithm.APRIORI);
        StatisticData spmfApriori = new StatisticData(RuleSource.SPMF, MineAlgorithm.APRIORI);
        StatisticData spmfClosed = new StatisticData(RuleSource.SPMF, MineAlgorithm.CLOSED_ASSOCIATION_RULES);
        StatisticData spmfFgpgrowth = new StatisticData(RuleSource.SPMF, MineAlgorithm.FPGrowth);
        StatisticData spmfSporadic = new StatisticData(RuleSource.SPMF, MineAlgorithm.SPORADIC);
        StatisticData spmfTopkclass = new StatisticData(RuleSource.SPMF, MineAlgorithm.TopKClass);
        StatisticData spmfTopk = new StatisticData(RuleSource.SPMF, MineAlgorithm.TopK);

        String[] nextRecord;
        while ((nextRecord = csvReader.readNext()) != null) {
            if (csvReader.getLinesRead() == 1) {
                continue;
            }
            String eventType = nextRecord[EVENT_TYPE_INDEX].replaceAll(":", "\\\\:");
            String actor1 = nextRecord[ACTOR1_INDEX].replaceAll(":", "\\\\:");
            String actor2 = nextRecord[ACTOR2_INDEX].replaceAll(":", "\\\\:");

            Page<RuleDocument> result = getRulesByActors(RuleSource.WEKA, MineAlgorithm.APRIORI, actor1, actor2);
            calculateStatistic(result, wekaStatistic, eventType);

            Page<RuleDocument> result1 = getRulesByActors(RuleSource.SPMF, MineAlgorithm.APRIORI, actor1, actor2);
            calculateStatistic(result1, spmfApriori, eventType);

            Page<RuleDocument> result2 = getRulesByActors(RuleSource.SPMF, MineAlgorithm.CLOSED_ASSOCIATION_RULES, actor1, actor2);
            calculateStatistic(result2, spmfClosed, eventType);

            Page<RuleDocument> result3 = getRulesByActors(RuleSource.SPMF, MineAlgorithm.FPGrowth, actor1, actor2);
            calculateStatistic(result3, spmfFgpgrowth, eventType);

            Page<RuleDocument> result4 = getRulesByActors(RuleSource.SPMF, MineAlgorithm.SPORADIC, actor1, actor2);
            calculateStatistic(result4, spmfSporadic, eventType);

            Page<RuleDocument> result5 = getRulesByActors(RuleSource.SPMF, MineAlgorithm.TopKClass, actor1, actor2);
            calculateStatistic(result5, spmfTopkclass, eventType);

            Page<RuleDocument> result6 = getRulesByActors(RuleSource.SPMF, MineAlgorithm.TopK, actor1, actor2);
            calculateStatistic(result6, spmfTopk, eventType);
        }

        statistics.add(wekaStatistic);
        statistics.add(spmfApriori);
        statistics.add(spmfClosed);
        statistics.add(spmfFgpgrowth);
        statistics.add(spmfSporadic);
        statistics.add(spmfTopkclass);
        statistics.add(spmfTopk);
        for (StatisticData statistic : statistics) {
            statistic.setTop1Match((double) statistic.getFirstMatchAll() / (double) csvReader.getLinesRead() * 100 + " %");
            statistic.setTop3Match((double) statistic.getTop3MatchAll() / (double) csvReader.getLinesRead() * 100 + " %");
            statistic.setTop5Match((double) statistic.getTop5MatchAll() / (double) csvReader.getLinesRead() * 100 + " %");
            statistic.setTop10Match((double) statistic.getTop10MatchAll() / (double) csvReader.getLinesRead() * 100 + " %");
        }
        logger.info("Statistic is ready.");
    }

    private Page<RuleDocument> getRulesByActors(RuleSource ruleSource, MineAlgorithm algorithm, String actor1, String actor2) {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .must(QueryBuilders.queryStringQuery(ruleSource.toString()).field(RuleDocument.SOURCE_PROPERTY))
                .must(QueryBuilders.queryStringQuery(algorithm.toString()).field(RuleDocument.ALGORITHM_PROPERTY));

        if (!actor1.equals("?")) {
            boolQueryBuilder.must(QueryBuilders.queryStringQuery(actor1).field(PREMISE_PROPERTY));
        }

        if (!actor2.equals(EMPTY_VALUE)) {
            boolQueryBuilder.must(QueryBuilders.queryStringQuery(actor2).field(PREMISE_PROPERTY));
        }

        boolQueryBuilder.must(QueryBuilders.queryStringQuery("EVENT_TYPE").field(CONSEQUENCE_PROPERTY));

        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("rules").withTypes("ruledocument")
                .withPageable(PageRequest.of(0, 10))
                .withSort(SortBuilders.fieldSort(RuleDocument.SUPPORT_PROPERTY).order(SortOrder.DESC))
                .withQuery(boolQueryBuilder)
                .build();

        Page<RuleDocument> result = ruleRepository.search(searchQuery);
        if (result.isEmpty() && actor1.equals(actor2)) {
            result = getRulesByActors(ruleSource, algorithm, actor1, EMPTY_VALUE);
            if (result.isEmpty()) {
                result = getRulesByActors(ruleSource, algorithm, EMPTY_VALUE, actor2);
            }
        }
        return result;
    }

    private void calculateStatistic(Page<RuleDocument> result, StatisticData statisticData, String eventType) {
        Optional<RuleDocument> anyResult = result.stream()
                .filter(r -> r.getConsequence().stream().anyMatch(c -> c.contains(eventType)))
                .findFirst();

        if (anyResult.isPresent()) {
            statisticData.incTop10Match();
            AtomicInteger index = new AtomicInteger(0);
            result.get().forEachOrdered(rule -> {
                if (anyResult.get() == rule) {
                    int i = index.intValue();
                    if (i == 0) {
                        statisticData.incFirstMatch();
                    }
                    if (i < 3) {
                        statisticData.incTop3Match();
                    }

                    if (i < 5) {
                        statisticData.incTop5Match();
                    }
                }
                index.incrementAndGet();
            });
        }
    }


    @Data
    @RequiredArgsConstructor
    public class StatisticData {

        @JsonIgnore
        private Long firstMatchAll = 0L;

        @JsonIgnore
        private Long top3MatchAll = 0L;

        @JsonIgnore
        private Long top5MatchAll = 0L;

        @JsonIgnore
        private Long top10MatchAll = 0L;

        private final RuleSource source;

        private final MineAlgorithm algorithm;

        private String top1Match;

        private String top3Match;

        private String top5Match;

        private String top10Match;

        void incFirstMatch() {
            firstMatchAll++;
        }

        void incTop3Match() {
            top3MatchAll++;
        }

        void incTop5Match() {
            top5MatchAll++;
        }

        void incTop10Match() {
            top10MatchAll++;
        }
    }
}

