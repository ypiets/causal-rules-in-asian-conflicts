package elastic.service;

import elastic.document.RuleDocument;
import elastic.event.RulesReadyEvent;
import elastic.repository.RuleRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RuleImportService {

    private final RuleRepository ruleRepository;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final Logger logger = LogManager.getLogger(this.getClass());

    @Value("${rules.weka.apriori}")
    private String wekaAprioriPath;

    @Value("${rules.spmf.apriori}")
    private String spmfAprioriPath;

    @Value("${rules.spmf.closed_association_rules}")
    private String spmfClosedPath;

    @Value("${rules.spmf.fpgrowth}")
    private String spmfFpgrowthPath;

    @Value("${rules.spmf.sporadic}")
    private String spmfSporadicPath;

    @Value("${rules.spmf.topkclass}")
    private String spmfTopkclassPath;

    @Value("${rules.spmf.topk}")
    private String spmfTopKPath;

    @Value("${rules.crdis.path}")
    private String crdidisPath;

    @Value("${rules.importFromFiles}")
    private Boolean importFromFiles;

    @Value("${rules.importIfRequired}")
    private Boolean importIfRequired;

    @Async
    @EventListener(classes = ApplicationReadyEvent.class)
    public void onStartup() throws IOException {
        if (importFromFiles) {
            migrate();
            return;
        }
        if (importIfRequired && ruleRepository.count() == 0) {
            migrate();
        }
        logger.info("Rules amount: " + ruleRepository.count());
        applicationEventPublisher.publishEvent(new RulesReadyEvent(this));
    }

    @Async
    public void migrate() throws IOException {
        logger.info("Saving rules to elasticsearch");
        readRuleAndSave(wekaAprioriPath, RuleDocument.RuleSource.WEKA, RuleDocument.MineAlgorithm.APRIORI, this::convertSPMFRule);
        readRuleAndSave(spmfAprioriPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.APRIORI, this::convertSPMFRule);
        readRuleAndSave(spmfClosedPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.CLOSED_ASSOCIATION_RULES, this::convertSPMFRule);
        readRuleAndSave(spmfFpgrowthPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.FPGrowth, this::convertSPMFRule);
        readRuleAndSave(spmfSporadicPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.SPORADIC, this::convertSPMFRule);
        readRuleAndSave(spmfTopkclassPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.TopKClass, this::convertSPMFRule);
        readRuleAndSave(spmfTopKPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.TopK, this::convertSPMFRule);
//        readRuleAndSave(crdidisPath, RuleDocument.RuleSource.SPMF, RuleDocument.MineAlgorithm.ZSCORE, this::convertCrdisRule);
        logger.info("Saved rules to elasticsearch");
    }

    private void readRuleAndSave(String fileName, RuleDocument.RuleSource ruleSource, RuleDocument.MineAlgorithm algorithm, Function<String, RuleDocument> converter) throws IOException {
        Resource resource = new ClassPathResource(fileName);
        List<RuleDocument> rules = Files.lines(resource.getFile().toPath())
                .map(converter)
                .peek(r -> r.setSource(ruleSource))
                .peek(r -> r.setAlgorithm(algorithm))
                .collect(Collectors.toList());

        ruleRepository.saveAll(rules);
    }

    private RuleDocument convertSPMFRule(String stringRule) {
        RuleDocument rule = converRule(stringRule);

        String[] split = stringRule.trim().split("==>");
        String left = split[0];
        String right = split[1];

        rule.setPremise(parseSPMF(left.trim()));
        rule.setConsequence(parseSPMF(right.trim()));
        rule.setConfidence(parseSPMFConfidence(stringRule));
        rule.setSupport(parserSPMFSupport(stringRule));
        return rule;
    }

    private Double parseSPMFConfidence(String stringRule) {
        String stringConfidence = stringRule.split("#CONF: ")[1];
        if (NumberUtils.isCreatable(stringConfidence)) {
            return Double.valueOf(stringConfidence);
        }
        return null;
    }

    private Integer parserSPMFSupport(String stringRule) {
        String stringSupport = stringRule.split(" #SUP: ")[1].split(" ")[0];
        if (NumberUtils.isCreatable(stringSupport)) {
            return Integer.valueOf(stringSupport);
        }
        return null;
    }

    private RuleDocument convertCrdisRule(String stringRule) {
        RuleDocument rule = new RuleDocument();
        rule.setFullText(stringRule);
        // TODO: 16/12/2018 hardcoded value
        return rule;
    }

    private RuleDocument converRule(String stringRule) {
        stringRule = stringRule.trim();
        RuleDocument rule = new RuleDocument();
        rule.setFullText(stringRule);

        String[] split = stringRule.trim().split("==>");
        String left = split[0];
        String right = split[1];

        rule.setPremise(parsePart(left));
        rule.setConsequence(parsePart(right));
        return rule;
    }

    private List<String> parsePart(String s) {
        String[] split = s.split(",");
        return Arrays.stream(split)
                .map(String::trim)
                .map(ss -> ss.split("]")[0])
                .map(ss -> ss.replaceAll("\\[", ""))
                .collect(Collectors.toList());
    }

    private List<String> parseSPMF(String s) {
        int right;
        int left = right = s.indexOf('=');

        if (s.isEmpty() || left < 0) {
            return new ArrayList<>();
        }

        while (left > 0 && s.charAt(left) != '\'' && s.charAt(left) != ' ' && s.charAt(left) != '>') {
            left--;
        }

        char delimiter = ' ';
        if (s.charAt(++right) == '\'') {
            right++;
            delimiter = '\'';
        }
        while (right < s.length() && s.charAt(right) != delimiter && s.charAt(right) != '#') {
            right++;
        }

        String substring = s.substring(left, right).trim().split("]")[0].replaceAll("\\[", "");
        List<String> strings = parseSPMF(s.substring(right));
        strings.add(substring);
        return strings;
    }
}
