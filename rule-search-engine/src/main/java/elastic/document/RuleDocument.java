package elastic.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Data
@NoArgsConstructor
@Document(indexName = "rules")
public class RuleDocument {

    public static final String SOURCE_PROPERTY = "source";
    public static final String ALGORITHM_PROPERTY = "algorithm";
    public static final String PREMISE_PROPERTY = "premise";
    public static final String CONSEQUENCE_PROPERTY = "consequence";
    public static final String FULL_TEXT_PROPERTY = "fullText";
    public static final String CONFIDENCE_PROPERTY = "confidence";
    public static final String SUPPORT_PROPERTY = "support";

    public enum RuleSource {
        WEKA, SPMF, CRDFDS
    }

    public enum MineAlgorithm {
        APRIORI, CLOSED_ASSOCIATION_RULES, FPGrowth, SPORADIC, TopKClass, TopK, ZSCORE
    }

    @Id
    private Long id = generatedId++;

    @JsonProperty(SOURCE_PROPERTY)
    private String source;

    @JsonProperty(ALGORITHM_PROPERTY)
    private String algorithm;

    @JsonProperty(PREMISE_PROPERTY)
    private List<String> premise;

    @JsonProperty(CONSEQUENCE_PROPERTY)
    private List<String> consequence;

    @JsonProperty(FULL_TEXT_PROPERTY)
    private String fullText;

    @JsonProperty(CONFIDENCE_PROPERTY)
    private Double confidence;

    @JsonProperty(SUPPORT_PROPERTY)
    private Integer support;

    public static Long generatedId = 0L;

    public void setId(Long id) {
        this.id = id;
        if (id >= generatedId) {
            generatedId = id;
            generatedId++;
        }
    }

    public void setSource(RuleSource ruleSource) {
        this.source = ruleSource.toString();
    }

    public void setAlgorithm(MineAlgorithm mineAlgorithm) {
        this.algorithm = mineAlgorithm.toString();
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RuleDocument)) {
            return false;
        }
        return ObjectUtils.nullSafeEquals(getId(), ((RuleDocument) obj).getId());
    }
}
