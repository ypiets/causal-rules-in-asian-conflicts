import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Collectors;

public class ConverterApplication {

    public static void main(String[] args) throws IOException {
        Files.write(
                new File("spmf_out_violence").toPath(),
                Files.lines(new File("spmf_out").toPath())
                        .filter(line -> line.matches(".*==>.*violence.*"))
                        .collect(Collectors.toList())
        );
    }
}
