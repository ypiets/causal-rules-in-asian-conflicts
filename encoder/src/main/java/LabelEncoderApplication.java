import com.google.gson.Gson;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LabelEncoderApplication {

    private final static String INPUT_FILE_NAME = "spmf_in.csv";
    private static final String CONVERTED_OUTPUT = "converted_output";
    private static final String ENCODED_JSON = "encoded.json";
    private static final String CONVERTED_INPUT_CSV = "converted_input.csv";

    private final static int EVENT_TYPE_INDEX = 0;
    private final static int ACTOR1_INDEX = 1;
    private final static int ACTOR2_INDEX = 2;

    public static void main(String[] args) throws IOException {
        new LabelEncoderApplication().decode();
    }

    private void encode() throws IOException {
        Reader reader = Files.newBufferedReader(new File(INPUT_FILE_NAME).toPath());
        CSVReader csvReader = new CSVReader(reader);

        Map<String, Integer> encodedMap = new HashMap<>();

        int eventCounter = 1;
        int actor1Counter = 100_000;
        int actor2Counter = 100_000_000;
        String[] nextRecord;
        List<String> convertedRecords = new LinkedList<>();
        while ((nextRecord = csvReader.readNext()) != null) {
            if (csvReader.getLinesRead() == 1) {
                convertedRecords.add(nextRecord[0] + "," + nextRecord[1] + "," + nextRecord[2]);
                continue;
            }
            String convertedRecord = eventCounter + "," + actor1Counter + "," + actor2Counter;
            convertedRecords.add(convertedRecord);

            if (!encodedMap.containsKey(nextRecord[EVENT_TYPE_INDEX])) {
                encodedMap.put(nextRecord[EVENT_TYPE_INDEX], eventCounter);
                eventCounter++;
            }

            if (!encodedMap.containsKey(nextRecord[ACTOR1_INDEX])) {
                encodedMap.put(nextRecord[ACTOR1_INDEX], actor1Counter);
                actor1Counter++;
            }

            if (!encodedMap.containsKey(nextRecord[ACTOR2_INDEX])) {
                encodedMap.put(nextRecord[ACTOR2_INDEX], actor2Counter);
                actor2Counter++;
            }
        }

        encodedMap.remove("?");

        Map<Integer, String> invertedMap = encodedMap.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));

        Gson gson = new Gson();
        String encodedJson = gson.toJson(invertedMap);

        File encodedJsonFile = new File(ENCODED_JSON);
        Files.write(encodedJsonFile.toPath(), Collections.singletonList(encodedJson));

        File file = new File(CONVERTED_INPUT_CSV);
        Files.write(file.toPath(), convertedRecords);
    }

    private void decode() throws IOException {
        File encodedJson = new File(ENCODED_JSON);
        String json = String.join("", Files.readAllLines(encodedJson.toPath()));
        Gson gson = new Gson();
        HashMap<String, String> codes = gson.fromJson(json, HashMap.class);

        File convertedOutput = new File(CONVERTED_OUTPUT);
        List<String> decoded = Files.lines(convertedOutput.toPath())
                .map(r -> {
                    for (String s : r.split("[ \t{}]")) {
                        if (NumberUtils.isDigits(s)) {
                            if (codes.containsKey(s)) {
                                String value = codes.get(s);
                                r = r.replaceAll(s, value);
                            }
                        }
                    }
                    return r;
                })
                .collect(Collectors.toList());

        Files.write(new File("decoded_output").toPath(), decoded);
    }

}
