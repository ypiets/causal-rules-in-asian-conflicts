In order to run rule-search-engine, enter following commands in the command prompt.
```
elasticsearch-6.5.3\bin\elasticsearch
gradlew rule-search-engine:bootRun
```
